const express = require('express')
const https = require('https');
var request = require('request');
const app = express();
var aws      = require('aws-sdk');
var receipt  = "";
let jsonData = require('./property.json')
const port = jsonData.port;
const queueUrl = jsonData.queueUrl;

// Load your AWS credentials and try to instantiate the object.
aws.config.loadFromPath(__dirname + '/config.json');
var sqs = new aws.SQS();
// Instantiate SQS.

var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : jsonData.dbHost,
    user     : jsonData.dbUser,
    password : jsonData.dbPassword,
    database : jsonData.database
});

connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);
});


app.get('/', (req, res) => res.send('Hello World!1222'));
app.get('/test/', (req, res) => res.send('Hello World!'));

setInterval(function () {
    console.log('second passed');
    readSqs()
}, jsonData.cron_interval); //once a minute


function readSqs() {
    var params = {
        QueueUrl: queueUrl,
        MaxNumberOfMessages:jsonData.maximumMessageReadingCount,
        WaitTimeSeconds:jsonData.WaitTimeSeconds,
        VisibilityTimeout: jsonData.VisibilityTimeout // 10 min wait time for anyone else to process.
    };

    sqs.receiveMessage(params, function(err, data) {
        if(err) {
            console.log("this is error");
            console.log(err);
        }
        else if (data.Messages){
            // console.log("received data");
            console.log(data.Messages);
            var messages = data.Messages;
            messages.forEach(function(value){
                var body = value.Body;
                body = JSON.parse(body);
                var msg = body.MessageBody;
                var msgReceivers = body.to;
                var receiptHandler = value.ReceiptHandle;
                console.log(msg);
                msgReceivers.forEach(function(value){
                    var numberLength = value.length;
                    var countryCode = value.substring(numberLength-12, numberLength-10);
                    var number = value.substring(numberLength-9, numberLength);
                    console.log("country code is " + countryCode);
                    if (countryCode =="91"){
                        number = value.substring(numberLength-10, numberLength);
                        console.log(number);
                        sendMessageToIndia(number,msg,receiptHandler);
                    } else {
                        connection.query('SELECT * FROM partner_subscription WHERE msisdn = ' + number + ' AND subscription=1', function (error, results, fields) {
                            if (error) throw error;
                            else if (results.length ==0) {
                                console.log("result is empty");
                                subscribeMsisdn(number,msg,receiptHandler);

                            } else {
                                // console.log(results);
                                sendMessage(number,msg,receiptHandler);
                            }
                            // connected!
                        });
                        console.log(number);
                    }
                });
            });
        }
    });
};
function subscribeMsisdn(msisdn,msg, reference){

    var data = {
        "method": "WEB",
        "msisdn": "tel:+94" + msisdn
    };
    console.log(data);
    request({
        url: jsonData.partners.dialog.subscribeUrl,
        method: "POST",
        json: true,   // <--Very important!!!
        body: data,
        headers: {
            'Authorization':jsonData.partners.dialog.accessToken
        }
    }, function (error, response, body){
        // var subscriptionResponse = JSON.parse(body);
        console.log(body);
        if(!error){
            var subscriptionResponse = body;
            if (subscriptionResponse !='undefined' && subscriptionResponse.data !='undefined' && subscriptionResponse.data.subscribeResponse !='undefined'){
                var status = subscriptionResponse.statusCode;
                var serviceId = subscriptionResponse.data.subscribeResponse.serviceID;
                var subscriptionStatus = subscriptionResponse.data.subscribeResponse.status;
                if (subscriptionStatus =='SUBSCRIBED' || subscriptionStatus =='ALREADY_SUBSCRIBED'){
                    subscriptionStatus =1;
                } else {
                    subscriptionStatus =0;
                }

                let stmt = `INSERT INTO partner_subscription (msisdn, service_provider,subscription,serviceID,response)
            VALUES(?,?,?,?,?)`;
                let todo = [msisdn, 'DIALOG',subscriptionStatus,serviceId,JSON.stringify(subscriptionResponse)];

                connection.query(stmt, todo, (err, results, fields) => {
                    if (err) {
                        return console.error(err.message);
                    } else {
                        sendMessage(msisdn,msg,reference);
                    }
                    // get inserted id
                    console.log('Todo Id:' + results.insertId);
                });
            }
        }

    });

}
function sendMessage(msisdn, msg, reference){
    var deletePara = {
        QueueUrl: queueUrl,
        ReceiptHandle: reference
    };
    var data = {
        "outboundSMSMessageRequest": {
            "address":["tel:94"+msisdn],
            "senderAddress":"tel:94777171240",
            "outboundSMSTextMessage":{
                "message": msg
            },
            "clientCorrelator": "123456",

            "receiptRequest": {

                "callbackData": "some-data-useful-to-the-requester"

            },

            "senderName": null
        }
    };

    request({
        url: jsonData.partners.dialog.messagingUrl,
        method: "POST",
        json: true,
        body: data,
        headers: {
            'Authorization':jsonData.partners.dialog.accessToken
        }
    }, function (error, response, body){
        var messageQuery = `INSERT INTO trn_dialog_message (msisdn, delivery_status,message,date)
            VALUES(?,?,?,?)`;
        if (!error){
            var messageInertion = [msisdn, 1,msg, new Date()];
            connection.query(messageQuery, messageInertion, (err, results, fields) => {
                if (err) {
                    return console.error(err.message);
                } else {
                    sqs.deleteMessage(deletePara, function(err, data) {
                        if(err) {
                            console.log(err);
                        }
                        else {
                            console.log("delete the message with reference " + deletePara);
                        }
                    });
                }
            });
        } else {
            var messageInertion = [msisdn, 0,msg, new Date()];
            connection.query(messageQuery, messageInertion, (err, results, fields) => {
                if (err) {
                    return console.error(err.message);
                }
            });
        }

    });
}
function sendMessageToIndia(msisdn, msg, reference){
    var deletePara = {
        QueueUrl: queueUrl,
        ReceiptHandle: reference
    };
    var msgGatewayUrl= jsonData.partners.indian.messagingUrl;
    msgGatewayUrl = msgGatewayUrl + "&message=" + msg;
    msgGatewayUrl = msgGatewayUrl + "&mobiles=" + msisdn;

    if (typeof msg != "undefined" && msg != null){
        request(msgGatewayUrl, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log("message from indian api");
                console.log(body) // Print the google web page.
                sqs.deleteMessage(deletePara, function(err, data) {
                    if(err) {
                        console.log(err);
                    } else {
                        console.log("delete the message with reference " + deletePara);
                    }
                });
            }
        })
    }
}

app.listen(port, () => console.log(`Immunify Sending Message On Port ${port}!`));