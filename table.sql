CREATE TABLE partner_subscription (
    msisdn varchar(20),
    service_provider varchar(50),
    subscription int(5),
    response varchar(255),
    serviceID varchar(100)
);

CREATE TABLE `immunify_sms`.`trn_dialog_message` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `msisdn` VARCHAR(12) NOT NULL,
  `date` VARCHAR(45) NOT NULL,
  `delivery_status` INT(5) NOT NULL,
  `message` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));