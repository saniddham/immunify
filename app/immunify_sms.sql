use immunify_sms;
CREATE TABLE dialog_subscription (
    msisdn varchar(20),
    service_provider varchar(20),
    subscription int(5),
    response varchar(255),
    serviceID varchar (255),
    PRIMARY KEY( msisdn )
);

ALTER TABLE dialog_subscription  ADD serviceID VARCHAR(255);