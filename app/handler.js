'use strict';

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region
AWS.config.update({region: 'us-east-1'});

// Create an SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});
var request = require('request');

var queueURL = "https://sqs.us-east-1.amazonaws.com/946433397476/dev-sms-out-standard";

var mysql = require('mysql');

var connection = mysql.createConnection({
    host     : '54.172.252.29',
    user     : 'newuser',
    password : 'user_password',
    database : 'immunify_sms'
});

// connection.connect();

var params = {
    AttributeNames: [
        "SentTimestamp"
    ],
    MaxNumberOfMessages: 1,
    MessageAttributeNames: [
        "All"
    ],
    QueueUrl: queueURL,
    VisibilityTimeout: 20,
    WaitTimeSeconds: 0
};


module.exports.receiveMsg =   (event, context, callback) => {

    var callback = callback;
    sqs.receiveMessage(params, function(err, data) {
        if (err) {
            console.log("Receive Error", err);
            var body = data.Messages[0].Body;
            body = JSON.parse(body);
            callback(null, {
                statusCode: 200,
                body: JSON.stringify({
                    message: 'Error happened in receiving the message',
                    input: body,
                    to: body.to,
                    number: body.to[0],
                }),
            });
        } else if (data.Messages) {
            var dummy = data.Messages;
            var body = data.Messages[0].Body;
            body = JSON.parse(body);

            var msgReceiver = body.to[0];
            var msg = body.MessageBody;
            var msgGatewayUrl;

            if (msgReceiver){
                connection.connect();
                connection.query('SELECT * FROM dialog_subscription where msisdn=' + msgReceiver + 'and subscription = 1', function(err, rows, fields) {
                    if (!err) {
                        if (typeof msg != "undefined" && msg != null){
                            request(options, function (error, response, body) {
                                // if (!error && response.statusCode == 200) {
                                callback(null, {
                                    statusCode: 200,
                                    body: JSON.stringify({
                                        input: dummy,
                                        message : msgReceiver
                                    }),
                                });
                                // }
                            })
                        }
                    } else {
                        console.log('Error while performing Query.');
                    }
                });


                msgGatewayUrl = "https://ideabiz.lk/apicall/smsmessaging/v3/outbound/94777171240/requests";
                var data = {
                    "outboundSMSMessageRequest": {
                    "address": ["tel" + msgReceiver],
                    "senderAddress": "tel:94777171240",
                    "outboundSMSTextMessage": {
                        "message": msg
                    },
                    "clientCorrelator": "123456",
                    "receiptRequest": {
                        "callbackData": "some-data-useful-to-the-requester"
                    },
                    "senderName": null
                    }

                };

                var options = {
                    method: 'POST',
                    body: data,
                    json: true,
                    url: msgGatewayUrl,
                    headers: {
                        'Authorization':'Bearer e3f1bac9ec996da8ef840df8d46455f'
                    }
                };



            } else {
                msgGatewayUrl= "http://api.msg91.com/api/sendhttp.php?country=91&sender=TESTIN&route=4&authkey=236073ASarRxKL5b914e8c";
                msgGatewayUrl = msgGatewayUrl + "&message=" + msg;
                msgGatewayUrl = msgGatewayUrl + "&mobiles=" + msgReceivers[0];

                if (typeof msg != "undefined" && msg != null){
                    request(msgGatewayUrl, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            console.log(body) // Print the google web page.
                            // sqs.deleteMessage(deleteParams, function(err, data) {
                            //     if (err) {
                            //         console.log("Delete Error", err);
                            //     } else {
                            //         console.log("Message Deleted", data);
                            //     }
                            // });
                        }
                    })
                }
            }

        }
    });
};

module.exports.vendorMsgListen =   (event, context, callback) => {
    callback(null, {
        statusCode: 200,
        body: JSON.stringify({
            message: 'received the message successfully',
            status_code: 200,
            status: 'success'
        }),
        headers: {'Content-Type': 'application/json'}
    });
};
